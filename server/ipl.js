// To find matches per year
function findMatchesPerYear(matches){
    return matches.reduce((matchesPerYear,match) => {
        !matchesPerYear[match.season] ? matchesPerYear[match.season] = 1:matchesPerYear[match.season] += 1;
        return matchesPerYear},{});
}


// To find wins by team per year
function findwinsByTeamPerYear(matches){
    let uniqueYears = {};
    
    return Object.entries(matches.reduce(function(winsByTeamPerYear,match){
            let teamWon = match.winner;
            let season = match.season;
            // To store every unique year
            if(!uniqueYears[season]){                        
                uniqueYears[season] = "true";                
            }
            if(teamWon != ""){
                if(!winsByTeamPerYear[teamWon]){
                    winsByTeamPerYear[teamWon] = {};
                    winsByTeamPerYear[teamWon][season] = 1;
                }else if(!winsByTeamPerYear[teamWon][season]){
                    winsByTeamPerYear[teamWon][season] = 1;                    
                }
                else{
                    winsByTeamPerYear[teamWon][season] += 1;
                }
            }
            return winsByTeamPerYear;
        },{})).reduce((arrOfWinsByTeamPerYear,indivTeamWins)=>{
            let arr = [];
            arr.push(indivTeamWins[0]);
            let yearWithWinsObj = indivTeamWins[1];
            let uniqueYearsArr = Object.keys(uniqueYears);
            let updatedYearWithWinsObj = uniqueYearsArr.reduce((acc,uniqueYear) => {
                if(!acc[uniqueYear]){
                  acc[uniqueYear] = 0;
                }
                return acc;
              },yearWithWinsObj);            
            arr.push(updatedYearWithWinsObj);
            arrOfWinsByTeamPerYear.push(arr);
        return arrOfWinsByTeamPerYear;
        },[]);
}



// To get the array with match id's of given year
// To Filter matches of season year
function findMatchsIds(matches,year){
    return matches.filter(match => {
        return match.season === year;
    }).map(match => match.id);
}


//To find Extra Runs In 2016
//Iterate though deliveries
function findExtraRuns(matches,deliveries,year) {
    let matchsIdsOf2016 = findMatchsIds(matches,year);
    return deliveries.reduce(function (accumulator, delivery) {
        let extraRuns = parseInt(delivery.extra_runs);
        if (extraRuns > 0) {
            //store the match_id,bowling_team
            let matchId = delivery.match_id;
            let bowlingTeam = delivery.bowling_team;
            // Check the match id of delivery is in 2016 match ids
            if (matchsIdsOf2016.includes(matchId)) {
                // if bowling team not in object
                if (!accumulator[bowlingTeam]) {
                    accumulator[bowlingTeam] = extraRuns;
                } else {
                    accumulator[bowlingTeam] += extraRuns;
                }
            }
        }
        return accumulator;
    }, {});
}
    

//Top 10 economical bowlers in 2015
// First Calculate noOfRuns and noOfBalls of each bowler
function findTop10EconomicalBowlers(matches,deliveries,year) {
    let matchsIdsOf2015 = findMatchsIds(matches,year);
    return Object.entries(deliveries.reduce(function (allBowlersStats, delivery) {
        // Check season 2015
        if (matchsIdsOf2015.includes(delivery.match_id)) {
            let totalRuns = parseInt(delivery.total_runs);
            let byeRuns = parseInt(delivery.bye_runs);
            let legByeRuns = parseInt(delivery.legbye_runs);
            let wideRuns = parseInt(delivery.wide_runs);
            let noballRuns = parseInt(delivery.noball_runs);
            let bowlerName = delivery.bowler;
            let runsGivenByBowler = totalRuns - (byeRuns + legByeRuns);
            // Create bowlername key
            if (!allBowlersStats[bowlerName]) {
                allBowlersStats[bowlerName] = {};
                allBowlersStats[bowlerName]["noOfBalls"] = 0;
                allBowlersStats[bowlerName]["noOfRuns"] = 0;
            }
            // Check ball count
            if (wideRuns == 0 && noballRuns == 0) {
                allBowlersStats[bowlerName]["noOfBalls"] += 1;
            }
            // Calculate Runs
            allBowlersStats[bowlerName]["noOfRuns"] += runsGivenByBowler;
        }
        return allBowlersStats;
    }, {}))//To calculate economy of each bowler
        .reduce((allBowlersEconomy, bowlerStat) => {
            let allRuns = bowlerStat[1]["noOfRuns"];
            let totalOvers = bowlerStat[1]["noOfBalls"] / 6;
            let economy = allRuns / totalOvers;
            let obj = {};
            obj["bowlerName"] = bowlerStat[0];
            obj["economy"] = parseFloat(economy.toFixed(2));
            allBowlersEconomy.push(obj);
            return allBowlersEconomy;
        }, [])
        // Sort the bowlers based on their economy
        .sort((bowler1, bowler2) => bowler1.economy - bowler2.economy)
        // To print top print top 10 economical bowlers
        .splice(0, 10)
        .reduce((newJsonFormat,jsonItem)=>{
            newJsonFormat[jsonItem["bowlerName"]] = jsonItem["economy"];
            return newJsonFormat;
        },{});
}


module.exports = {
    findMatchesPerYear:findMatchesPerYear,
    findwinsByTeamPerYear :findwinsByTeamPerYear,
    findExtraRuns:findExtraRuns,
    findTop10EconomicalBowlers:findTop10EconomicalBowlers
};