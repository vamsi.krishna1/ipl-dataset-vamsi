const matchesFilePath = '../IPL-dataset/matches.csv'

const deliveriesFilePath = '../IPL-dataset/deliveries.csv'
// Load required modules to our application
const csv = require('csvtojson')
//to work with the file system on your computer.
var fs = require('fs');
var ipl = require('./ipl');

// Convert CSV file to JSON
csv()
    .fromFile(matchesFilePath)
    .then((jsonObj) => {
        //console.log(jsonObj);
        // Write JSON file to the desired location.
        //IPL-dataset
        fs.writeFileSync('../IPL-dataset/matches.json', JSON.stringify(jsonObj));
    })

// Read deliveries csv file
csv()
    .fromFile(deliveriesFilePath)
    .then((jsonObj) => {
        // Write JSON file to the desired location.
        fs.writeFileSync('../IPL-dataset/deliveries.json', JSON.stringify(jsonObj));
    })


//Read json file

let matchesRawData = fs.readFileSync('../IPL-dataset/matches.json',"utf8");
let matches = JSON.parse(matchesRawData);
// console.log(matches);

//Read json file
let deliveriesRawData = fs.readFileSync('../IPL-dataset/deliveries.json',"utf8");

let deliveries = JSON.parse(deliveriesRawData);

fs.writeFileSync('../Outcome/matchesPerYear.json', JSON.stringify(ipl.findMatchesPerYear(matches)));
fs.writeFileSync('../Outcome/winsByTeamPerYear.json', JSON.stringify(ipl.findwinsByTeamPerYear(matches)));
fs.writeFileSync('../Outcome/extraRunsIn2016.json', JSON.stringify(ipl.findExtraRuns(matches,deliveries,'2016')));
fs.writeFileSync('../Outcome/top10EconomicalBowlers.json', JSON.stringify(ipl.findTop10EconomicalBowlers(matches,deliveries,'2015')));
