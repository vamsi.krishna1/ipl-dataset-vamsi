/* eslint-disable no-undef */
function fetchMatchesPerYear(){
    fetch("../Outcome/matchesPerYear.json")
    .then(response => response.json())
    .then(json => {
        let xAxisTitle = 'Years';
        let graphTitle = 'No Of Matches Per Year';
        let yAxisTitle = 'No Of Matches';
        let toolTipY = 'Matches Played';
        let toolTipX = 'Year';
        displayChartOfMtachesPerYear(json,xAxisTitle,graphTitle,yAxisTitle,toolTipX,toolTipY);
    })
}

function fetchExtraRunsIn2016(){
    fetch("../Outcome/extraRunsIn2016.json")
    .then(response => response.json())
    .then(json => {
        let xAxisTitle = 'Team Names';
        let graphTitle = 'Extra Runs in 2016';
        let yAxisTitle = 'Extra Runs';
        let toolTipY = 'Extra Runs';
        let toolTipX = 'Team Name';
        displayChartOfMtachesPerYear(json,xAxisTitle,graphTitle,yAxisTitle,toolTipX,toolTipY);
    })
}

function fetchTop10EconomicalBowlers(){
    fetch("../Outcome/top10EconomicalBowlers.json")
    .then(response => response.json())
    .then(json => {
        let xAxisTitle = 'Bowler Names';
        let graphTitle = 'Top 10 Economical Bowlers in 2015';
        let yAxisTitle = 'Economy';
        let toolTipY = 'Economy';
        let toolTipX = 'Bowler';       
        displayChartOfMtachesPerYear(json,xAxisTitle,graphTitle,yAxisTitle,toolTipX,toolTipY);
    })
}

function findwinsByTeamPerYear(){    
    fetch("../Outcome/winsByTeamPerYear.json")
    .then(response => response.json())
    .then(json => {
        // let jsonKeyValues = Object.entries(json);
        // let xAxisTitle = (jsonKeyValues[0][1]);
        let xAxisTitle = (json[0][1])
        let series = json.reduce((seriesArr,jsonElement)=>{
            let obj = {};
            obj['name'] = jsonElement[0];
            obj['data'] = Object.values(jsonElement[1]);
            seriesArr.push(obj);
            return seriesArr;
        },[]);
        console.log(series);
        visualizeWinsByTeamPerYear(json,series,xAxisTitle);
    })
}


function visualizeWinsByTeamPerYear(data,newSeries,xAxisTitle){
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Matches Won By Team Per Year'
        },
        xAxis: {            
            categories: xAxisTitle,            
            title: {
                text: "Years"
            },
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Matches Won'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            align: 'center',
            x: 30,
            verticalAlign: 'bottom',
            y: 25,
            floating: false,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            },            
        },
        series:newSeries
    });
    }
    
    findwinsByTeamPerYear();


function displayChartOfMtachesPerYear(jsonData,xAxisTitle,graphTitle,yAxisTitle,toolTipX,toolTipY){
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: graphTitle
        },
        xAxis: {
           categories : Object.keys(jsonData),
           title: {
            text: xAxisTitle
        },
        },
        yAxis: {
            min: 0,
            title: {
                text: yAxisTitle
            },
            stackLabels: {
                // enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },        
        tooltip: {
            headerFormat: `<b>${toolTipX}:{point.x}</b><br/>`,
            pointFormat: '{series.name}: {point.y}'
        },        
            
        plotOptions: {
            column: {
                // Number at middle of each bar of graph
                // stacking: 'normal',
                // Number at top of each bar of graph
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name:toolTipY,
            data:Object.values(jsonData)     
        }]
    });

}

fetchMatchesPerYear();
fetchExtraRunsIn2016();
fetchTop10EconomicalBowlers();
