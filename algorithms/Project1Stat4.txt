//Top 10 economical bowlers in 2015
In deliveries json:
match_id: 518 to 576
bowler: bowler name

Bowler Economy : No of runs/ No of overs;

For byes and leg byes => bowler's ball count, Runs not credited to bowler account.

For wide balls, noballs =>bowler's ball does'nt count,  Runs credited to bowler account.

penalty_runs,batsman_runs => bowler's ball count,  Runs credited to bowler account.

runs in bowler account = totalRuns - byes and leg bye Runs
------------------------------------------------------------------------------------------------------
Create an empty dict to calculate all bowlers economy:(allBowlersEconomy)
// At the end calc no of overs and divide by no of runs and place
allBowlersEconomy = {
 Ashmin : {no_of_runs : 150; no_of_balls= 72, economy: 12.5},
}

For delivery in deliveries:
    // Check season 2015
    if delivery.match_id >= 518 and  delivery.match_id <= 576:
       let total_runs = delivery.total_runs;
       let bye_runs = delivery.bye_runs;
       let legbye_runs = delivery.legbye_runs;
       let wide_runs =delivery.wide_runs;
       let noball_runs =delivery.noball_runs;
       // Create bowlername key
       if bowler_name not in allBowlersEconomy:
          allBowlersEconomy[bowler_name] = {};
          allBowlersEconomy[bowler_name]["no_of_balls"] = 0;
          allBowlersEconomy[bowler_name]["no_of_runs"] = 0;
          allBowlersEconomy[bowler_name]["economy"] = 0;
       // Check ball count          
       if  wide_runs == 0 && noball_runs == 0:
           allBowlersEconomy[bowler_name]["no_of_balls"] += 1;
       // Check ball runs
       allBowlersEconomy[bowler_name]["no_of_runs"] = total_runs - bye_runs - delivery.legbye_runs;
//Calculate economy   
For every item in allBowlersEconomy:
      //calc economy
      let totalRuns = allBowlersEconomy[bowler_name]["no_of_runs"] ;
      let totalOvers = allBowlersEconomy[bowler_name]["no_of_runs"]/6;
      allBowlersEconomy[bowler_name]["economy"] =     totalRuns/ totalOvers;

// Print top 10 Economical bowlers:



