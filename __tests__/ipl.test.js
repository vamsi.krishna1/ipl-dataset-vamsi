/* eslint-disable no-undef */
let ipl = require("../server/ipl");

// Input Data for Test Cases
let matches = [{
    "id": "1",
    "season": "2017",
    "city": "Hyderabad",
    "date": "2017-04-05",
    "team1": "Sunrisers Hyderabad",
    "team2": "Royal Challengers Bangalore",
    "toss_winner": "Royal Challengers Bangalore",
    "toss_decision": "field",
    "result": "normal",
    "dl_applied": "0",
    "winner": "Sunrisers Hyderabad",
    "win_by_runs": "35",
    "win_by_wickets": "0",
    "player_of_match": "Yuvraj Singh",
    "venue": "Rajiv Gandhi International Stadium, Uppal",
    "umpire1": "AY Dandekar",
    "umpire2": "NJ Llong",
    "umpire3": ""
}, { "id": "523", "season": "2015", "city": "Delhi", "date": "2015-04-12", "team1": "Delhi Daredevils", "team2": "Rajasthan Royals", "toss_winner": "Rajasthan Royals", "toss_decision": "field", "result": "normal", "dl_applied": "0", "winner": "Rajasthan Royals", "win_by_runs": "0", "win_by_wickets": "3", "player_of_match": "DJ Hooda", "venue": "Feroz Shah Kotla", "umpire1": "SD Fry", "umpire2": "CB Gaffaney", "umpire3": "" },
{
    "id": "2",
    "season": "2017",
    "city": "Pune",
    "date": "2017-04-06",
    "team1": "Mumbai Indians",
    "team2": "Rising Pune Supergiants",
    "toss_winner": "Rising Pune Supergiants",
    "toss_decision": "field",
    "result": "normal",
    "dl_applied": "0",
    "winner": "Rising Pune Supergiants",
    "win_by_runs": "0",
    "win_by_wickets": "7",
    "player_of_match": "SPD Smith",
    "venue": "Maharashtra Cricket Association Stadium",
    "umpire1": "A Nand Kishore",
    "umpire2": "S Ravi",
    "umpire3": ""
}, {
    "id": "577",
    "season": "2016",
    "city": "Mumbai",
    "date": "2016-04-09",
    "team1": "Mumbai Indians",
    "team2": "Rising Pune Supergiants",
    "toss_winner": "Mumbai Indians",
    "toss_decision": "bat",
    "result": "normal",
    "dl_applied": "0",
    "winner": "Rising Pune Supergiants",
    "win_by_runs": "0",
    "win_by_wickets": "9",
    "player_of_match": "AM Rahane",
    "venue": "Wankhede Stadium",
    "umpire1": "HDPK Dharmasena",
    "umpire2": "CK Nandan",
    "umpire3": ""
},
{
    "id": "518",
    "season": "2015",
    "city": "Kolkata",
    "date": "2015-04-08",
    "team1": "Mumbai Indians",
    "team2": "Kolkata Knight Riders",
    "toss_winner": "Kolkata Knight Riders",
    "toss_decision": "field",
    "result": "normal",
    "dl_applied": "0",
    "winner": "Kolkata Knight Riders",
    "win_by_runs": "0",
    "win_by_wickets": "7",
    "player_of_match": "M Morkel",
    "venue": "Eden Gardens",
    "umpire1": "S Ravi",
    "umpire2": "C Shamshuddin",
    "umpire3": ""
},
{
    "id": "458",
    "season": "2014",
    "city": "Abu Dhabi",
    "date": "2014-04-16",
    "team1": "Kolkata Knight Riders",
    "team2": "Mumbai Indians",
    "toss_winner": "Kolkata Knight Riders",
    "toss_decision": "bat",
    "result": "normal",
    "dl_applied": "0",
    "winner": "Kolkata Knight Riders",
    "win_by_runs": "41",
    "win_by_wickets": "0",
    "player_of_match": "JH Kallis",
    "venue": "Sheikh Zayed Stadium",
    "umpire1": "M Erasmus",
    "umpire2": "RK Illingworth",
    "umpire3": ""
},
{
    "id": "382",
    "season": "2013",
    "city": "Kolkata",
    "date": "2013-04-03",
    "team1": "Delhi Daredevils",
    "team2": "Kolkata Knight Riders",
    "toss_winner": "Kolkata Knight Riders",
    "toss_decision": "field",
    "result": "normal",
    "dl_applied": "0",
    "winner": "Kolkata Knight Riders",
    "win_by_runs": "0",
    "win_by_wickets": "6",
    "player_of_match": "SP Narine",
    "venue": "Eden Gardens",
    "umpire1": "S Ravi",
    "umpire2": "SJA Taufel",
    "umpire3": ""
},
{
    "id": "308",
    "season": "2012",
    "city": "Chennai",
    "date": "2012-04-04",
    "team1": "Chennai Super Kings",
    "team2": "Mumbai Indians",
    "toss_winner": "Mumbai Indians",
    "toss_decision": "field",
    "result": "normal",
    "dl_applied": "0",
    "winner": "Mumbai Indians",
    "win_by_runs": "0",
    "win_by_wickets": "8",
    "player_of_match": "RE Levi",
    "venue": "MA Chidambaram Stadium, Chepauk",
    "umpire1": "JD Cloete",
    "umpire2": "SJA Taufel",
    "umpire3": ""
},
{
    "id": "235",
    "season": "2011",
    "city": "Chennai",
    "date": "2011-04-08",
    "team1": "Chennai Super Kings",
    "team2": "Kolkata Knight Riders",
    "toss_winner": "Chennai Super Kings",
    "toss_decision": "bat",
    "result": "normal",
    "dl_applied": "0",
    "winner": "Chennai Super Kings",
    "win_by_runs": "2",
    "win_by_wickets": "0",
    "player_of_match": "S Anirudha",
    "venue": "MA Chidambaram Stadium, Chepauk",
    "umpire1": "BR Doctrove",
    "umpire2": "PR Reiffel",
    "umpire3": ""
}, {
    "id": "175",
    "season": "2010",
    "city": "Mumbai",
    "date": "2010-03-12",
    "team1": "Kolkata Knight Riders",
    "team2": "Deccan Chargers",
    "toss_winner": "Deccan Chargers",
    "toss_decision": "field",
    "result": "normal",
    "dl_applied": "0",
    "winner": "Kolkata Knight Riders",
    "win_by_runs": "11",
    "win_by_wickets": "0",
    "player_of_match": "AD Mathews",
    "venue": "Dr DY Patil Sports Academy",
    "umpire1": "RE Koertzen",
    "umpire2": "RB Tiffin",
    "umpire3": ""
}];

//Some Random Deliveries
let someRandomDeliveries = [
    {
        match_id: '11',
        inning: '2',
        batting_team: 'Kolkata Knight Riders',
        bowling_team: 'Kings XI Punjab',
        over: '17',
        ball: '3',
        batsman: 'MK Pandey',
        non_striker: 'G Gambhir',
        bowler: 'MP Stoinis',
        is_super_over: '0',
        wide_runs: '0',
        bye_runs: '0',
        legbye_runs: '0',
        noball_runs: '0',
        penalty_runs: '0',
        batsman_runs: '6',
        extra_runs: '0',
        total_runs: '6',
        player_dismissed: '',
        dismissal_kind: '',
        fielder: ''
    },
    {
        match_id: '124',
        inning: '2',
        batting_team: 'Royal Challengers Bangalore',
        bowling_team: 'Deccan Chargers',
        over: '20',
        ball: '5',
        batsman: 'P Kumar',
        non_striker: 'A Kumble',
        bowler: 'RP Singh',
        is_super_over: '0',
        wide_runs: '0',
        bye_runs: '0',
        legbye_runs: '0',
        noball_runs: '0',
        penalty_runs: '0',
        batsman_runs: '1',
        extra_runs: '0',
        total_runs: '1',
        player_dismissed: '',
        dismissal_kind: '',
        fielder: ''
    },
    {
        match_id: '518',
        inning: '2',
        batting_team: 'Kolkata Knight Riders',
        bowling_team: 'Mumbai Indians',
        over: '19',
        ball: '3',
        batsman: 'SA Yadav',
        non_striker: 'YK Pathan',
        bowler: 'R Vinay Kumar',
        is_super_over: '0',
        wide_runs: '0',
        bye_runs: '0',
        legbye_runs: '0',
        noball_runs: '0',
        penalty_runs: '0',
        batsman_runs: '4',
        extra_runs: '0',
        total_runs: '4',
        player_dismissed: '',
        dismissal_kind: '',
        fielder: ''
    },
    {
        match_id: '523',
        inning: '2',
        batting_team: 'Rajasthan Royals',
        bowling_team: 'Delhi Daredevils',
        over: '20',
        ball: '5',
        batsman: 'CH Morris',
        non_striker: 'TG Southee',
        bowler: 'AD Mathews',
        is_super_over: '0',
        wide_runs: '0',
        bye_runs: '0',
        legbye_runs: '0',
        noball_runs: '0',
        penalty_runs: '0',
        batsman_runs: '1',
        extra_runs: '0',
        total_runs: '1',
        player_dismissed: '',
        dismissal_kind: '',
        fielder: ''
    },
    {
        match_id: '564',
        inning: '2',
        batting_team: 'Kings XI Punjab',
        bowling_team: 'Sunrisers Hyderabad',
        over: '20',
        ball: '5',
        batsman: 'DA Miller',
        non_striker: 'BE Hendricks',
        bowler: 'I Sharma',
        is_super_over: '0',
        wide_runs: '0',
        bye_runs: '0',
        legbye_runs: '0',
        noball_runs: '0',
        penalty_runs: '0',
        batsman_runs: '0',
        extra_runs: '0',
        total_runs: '0',
        player_dismissed: '',
        dismissal_kind: '',
        fielder: ''
    },
    {
        match_id: '577',
        inning: '1',
        batting_team: 'Mumbai Indians',
        bowling_team: 'Rising Pune Supergiants',
        over: '4',
        ball: '4',
        batsman: 'HH Pandya',
        non_striker: 'LMP Simmons',
        bowler: 'I Sharma',
        is_super_over: '0',
        wide_runs: '0',
        bye_runs: '0',
        legbye_runs: '1',
        noball_runs: '0',
        penalty_runs: '0',
        batsman_runs: '0',
        extra_runs: '1',
        total_runs: '1',
        player_dismissed: '',
        dismissal_kind: '',
        fielder: ''
    },
    {
        match_id: '577',
        inning: '1',
        batting_team: 'Mumbai Indians',
        bowling_team: 'Rising Pune Supergiants',
        over: '6',
        ball: '3',
        batsman: 'KA Pollard',
        non_striker: 'AT Rayudu',
        bowler: 'I Sharma',
        is_super_over: '0',
        wide_runs: '5',
        bye_runs: '0',
        legbye_runs: '0',
        noball_runs: '0',
        penalty_runs: '0',
        batsman_runs: '0',
        extra_runs: '5',
        total_runs: '5',
        player_dismissed: '',
        dismissal_kind: '',
        fielder: ''
    },
    {
        match_id: '577',
        inning: '2',
        batting_team: 'Rising Pune Supergiants',
        bowling_team: 'Mumbai Indians',
        over: '6',
        ball: '5',
        batsman: 'AM Rahane',
        non_striker: 'F du Plessis',
        bowler: 'JJ Bumrah',
        is_super_over: '0',
        wide_runs: '0',
        bye_runs: '0',
        legbye_runs: '0',
        noball_runs: '1',
        penalty_runs: '0',
        batsman_runs: '1',
        extra_runs: '1',
        total_runs: '2',
        player_dismissed: '',
        dismissal_kind: '',
        fielder: ''
    },
    {
        match_id: '577',
        inning: '2',
        batting_team: 'Rising Pune Supergiants',
        bowling_team: 'Mumbai Indians',
        over: '7',
        ball: '5',
        batsman: 'F du Plessis',
        non_striker: 'AM Rahane',
        bowler: 'R Vinay Kumar',
        is_super_over: '0',
        wide_runs: '1',
        bye_runs: '0',
        legbye_runs: '0',
        noball_runs: '0',
        penalty_runs: '0',
        batsman_runs: '0',
        extra_runs: '1',
        total_runs: '1',
        player_dismissed: '',
        dismissal_kind: '',
        fielder: ''
    }
];



describe("To Test Function findMatchesPerYear", () => {
    test('To test if function exists', () => {
        expect(ipl.findMatchesPerYear).toBeDefined();
    });

    test('To test for empty input', () => {
        expect(ipl.findMatchesPerYear([])).toEqual({});
    });

    test('To test Output for Sample Data', () => {
        expect(ipl.findMatchesPerYear(matches)).toEqual({
            "2017": 2, "2016": 1, "2015": 2, "2014": 1, "2013": 1, "2012": 1, "2011": 1, "2010": 1
        });
    })
});

describe("To Test Function findwinsByTeamPerYear", () => {
    test("To test if function exists", () => {
        expect(ipl.findwinsByTeamPerYear).toBeDefined();
    });

    test("To test for empty input", () => {
        expect(ipl.findwinsByTeamPerYear([])).toEqual([]);
    });

    test("To test Output for Sample Data", () => {
        expect(ipl.findwinsByTeamPerYear(matches)).toEqual([
            [
              "Sunrisers Hyderabad",
              {
                "2010": 0,
                "2011": 0,
                "2012": 0,
                "2013": 0,
                "2014": 0,
                "2015": 0,
                "2016": 0,
                "2017": 1
              }
            ],
            [
              "Rajasthan Royals",
              {
                "2010": 0,
                "2011": 0,
                "2012": 0,
                "2013": 0,
                "2014": 0,
                "2015": 1,
                "2016": 0,
                "2017": 0
              }
            ],
            [
              "Rising Pune Supergiants",
              {
                "2010": 0,
                "2011": 0,
                "2012": 0,
                "2013": 0,
                "2014": 0,
                "2015": 0,
                "2016": 1,
                "2017": 1
              }
            ],
            [
              "Kolkata Knight Riders",
              {
                "2010": 1,
                "2011": 0,
                "2012": 0,
                "2013": 1,
                "2014": 1,
                "2015": 1,
                "2016": 0,
                "2017": 0
              }
            ],
            [
              "Mumbai Indians",
              {
                "2010": 0,
                "2011": 0,
                "2012": 1,
                "2013": 0,
                "2014": 0,
                "2015": 0,
                "2016": 0,
                "2017": 0
              }
            ],
            [
              "Chennai Super Kings",
              {
                "2010": 0,
                "2011": 1,
                "2012": 0,
                "2013": 0,
                "2014": 0,
                "2015": 0,
                "2016": 0,
                "2017": 0
              }
            ]
          ]
          );
    });
});

describe("To Test Function findExtraRuns", () => {
    test("To test if function exists", () => {
        expect(ipl.findExtraRuns).toBeDefined();
    });

    test("To test for empty input", () => {
        expect(ipl.findExtraRuns([], [])).toEqual({});
    });


    test("To test Output for Sample Data", () => {
        expect(ipl.findExtraRuns(matches, someRandomDeliveries, 2016)).toEqual({
            "Rising Pune Supergiants": 6,
            "Mumbai Indians": 2
        });
    });
});

describe("To Test Function findTop10EconomicalBowlers", () => {
    test("To test if function exists", () => {
        expect(ipl.findTop10EconomicalBowlers).toBeDefined();
    });

    test("To test for empty input", () => {
        expect(ipl.findTop10EconomicalBowlers([], [])).toEqual({});
    });

    test("To test with Sample Data", () => {
        expect(ipl.findTop10EconomicalBowlers(matches, someRandomDeliveries, '2015')).toEqual({ "AD Mathews": 6, "R Vinay Kumar": 24 });
    });

});